/**
 * Library to create a table from array of arrays
 * The first version include possibility to generate a gradiant
 * color from top left to bottom right
 */ 


 //To use this lib you need to set array like
 let tab = [
     /*th of table*/
     ["Name","Firstname","xxx"],
     /*tr of table*/
     ["Foo","Bar","etc.."] 
     /*You can add as much td as you want with as much tr as you want*/
    ]
//Then if you want a gradiant effect on your html table you need to set 2 hex color
let red = "#FF0000";
let blue = "#0000FF";
//Then use the function "main" and replace args buy your variables
/**
 * 
 * Here start the lib code
 * 
 */
//Calculate steps
function stepX(tab) {
    let steps = 0;
    for (i = 0; i <tab.length; i++) {
        steps += tab[0].length;
    }
    return steps;
}
//Generate gradient
function generateGradiant(col,col1,step) {
    var gradient = getGradiant(col,col1,step);
    return gradient;
}
//Generate table
function generateTable(gradiant,tab) {
    let l = 0;
    let table = $("<table/>");
    let thead = $("<thead/>");
    let tbody = $("<tbody/>");
    $(".table").append(table);
    table.append(thead,tbody);
    /* table.append(tbody); */
    for (j = 0; j < tab[0].length; j++) {
        let th = $("<th/>");
        if (typeof gradiant != "undefined") {
            th.css({/* "border":"1px solid", */"background": gradiant[l]});
        }
        th.text(tab[0][j]);
        thead.append(th);
        l++
    }
    let m = 0;
    for (i = 1; i < tab.length; i++) {
        let tr = $("<tr/>");
        tbody.append(tr);
        for (k = 0; k < tab[i].length; k++) {
            let td = $("<td/>");
            if (typeof gradiant != "undefined") {
                td.css({/* "border":"1px solid", */"background": gradiant[l]});
            }
            td.text(tab[i][k]);
            tr.append(td);
            l++
        }
    }
}
/**
 * 
 * Function to use in order to construct the HTML table
 * 
 */
function main(tab,col,col1) {
    if (typeof col != "undefined" && typeof col1 != "undefined") {
        var step = stepX(tab);
        var gradiant = generateGradiant(col,col1,step);
    }
    var table = generateTable(gradiant,tab);
    return table;
}
/**
 * 
 * 
 * 
 */

//Conditions for choice if a Math.floor or a Math.ceil is required
function swtichNegOrPos(a) {
    (a >= 0) ? a = Math.floor(a): a = Math.ceil(a);
    return a;
}
//Convert hex to rgb
function convert(color1,color2,step) {
    //Call hextorbg for each color
    rgb1 = hexToRgb(color1);
    rgb2 = hexToRgb(color2);
    return result = [rgb1,rgb2,step];
    /* stepS(rgb1, rgb2, step); */
}
//Remove # from hex
function cutHex(h) {
    return (h.charAt(0)=="#") ? h.substring(1,7):h
}
//Convert hex color to rgb color and cut strings
function hexToRgb(hex) {
    //Create variables from hex to rgb separating colors
    var red = parseInt((cutHex(hex)).substring(0,2), 16);
    var green = parseInt((cutHex(hex)).substring(2,4), 16);
    var blue = parseInt((cutHex(hex)).substring(4,6), 16);
    return rgb = [red, green, blue];
}
//Calculate steps size
function stepS (result) {
    var difRed = (result[1][0] - result[0][0])/result[2];
    var difGreen = (result[1][1] - result[0][1])/result[2];
    var difBlue = (result[1][2] - result[0][2])/result[2];
    difRed = swtichNegOrPos(difRed);
    difGreen = swtichNegOrPos(difGreen);
    difBlue = swtichNegOrPos(difBlue);
    return difColors = [difRed,difGreen,difBlue,result];
}
//Generate gradiant
function increment(difColors) {
    let resRgb = []
    //LOOP FOR ALL COLORS
    for (j=0; j<difColors[3][2]; j++) {
            difColors[3][0][0] += difColors[0];
            difColors[3][0][1] += difColors[1];
            difColors[3][0][2] += difColors[2];
            resRgb[j] = rgbToHex(difColors[3][0][0], difColors[3][0][1], difColors[3][0][2]);
    }
    return resRgb;
}
//Convert RGB color to HEX color
function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}
//HEX convertion to string
function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}
//Get the gradiant colors
function getGradiant(color1,color2,step) {
    var conv = convert(color1,color2,step)
    var ste = stepS(conv);
    var incre = increment(ste);
    return incre;
}
//Import from JSON file on your PC
function importJSON(elemId) {   
    document.getElementById(elemId).onchange = function(evt) {
        try {
            let files = evt.target.files;
            if (!files.length) {
                alert('No file selected!');
                return;
            }
            let file = files[0];
            let reader = new FileReader();
            const self = this;
            reader.onload = (event) => {
                console.log('FILE CONTENT', event.target.result);
            };
            return reader.readAsText(file);
        } catch (err) {
            console.error(err);
        }
    }
 }
 //Function to show if a file is ready to load
 function readyOrNot(reader,elemClass){
    var x = reader
    var txt = "";
    if ('files' in x) {
        if (x.files.length == 0) {
            txt = "Selection un fichier JSON.";
        } else {
            for (var i = 0; i < x.files.length; i++) {
                txt += "<br><strong>" + (i+1) + ". fichier JSON</strong><br>";
                var file = x.files[i];
                console.log(file);
                if ('name' in file) {
                    txt += "Nom: " + file.name + "<br>";
                }
                if ('size' in file) {
                    txt += "Taille: " + file.size + " bytes <br>";
                }
            }
        }
    } 
    console.log(txt);
    $("."+elemClass).html(txt);
  }